package com.example.itschool.circlediagram;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    CircleDiagram diagram, diagram1, diagram2;
    int[] data = {512,1241,5150,152};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        diagram = (CircleDiagram) findViewById(R.id.diagram);

        diagram1 = (CircleDiagram) findViewById(R.id.diagram1);
        diagram2 = (CircleDiagram) findViewById(R.id.diagram2);


        diagram1.setData(data);
        diagram2.setData(data);
        diagram.setData(data);
    }

}
