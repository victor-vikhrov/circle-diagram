package com.example.itschool.circlediagram;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by IT SCHOOL on 26.11.2016.
 */
public class CircleDiagram extends View {

    private static final String LOG_TAG = "Circle Diagram";
    private float[] data = new float[1];

    private int[] colors = {Color.BLACK, Color.GREEN, Color.BLUE, Color.RED, Color.YELLOW, Color.MAGENTA};

    public CircleDiagram(Context context) { //
        super(context);
    }

    public CircleDiagram(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData (int[] array) { // принимаем входные данные
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        float persent = sum / 100.0f;
        float[] persents = new float[array.length];
        for (int i = 0; i < array.length; i++) {
            persents[i] = array[i] / persent;
            Log.d(LOG_TAG, persents[i] + "");
        }
        data = persents;

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawARGB(255, 255, 255, 255); // закрашиваем фон
        float width = canvas.getWidth(); // ширина canvas
        float height = canvas.getHeight(); // высота канвас

        // вычисляем центр canvas
        float centerX = width / 2;
        float centerY = height / 2;

        float radius = Math.min(width, height) / 4; // радиус круговой диаграмы

        RectF rect = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
        Paint paint = new Paint();

        float currentAngle = 0;
        float percentAngle = 360.0f / 100.0f;
        for (int i = 0; i < data.length; i++) {
            paint.setColor(colors[i]);
            Log.d(LOG_TAG, "currentAngle:" + currentAngle + " percents:" + data[i]);
            float angle = data[i] * percentAngle;
            canvas.drawArc(rect, currentAngle, angle, true, paint);
            currentAngle += angle;
        }




    }
}
